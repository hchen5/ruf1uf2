using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "PlayerHP", menuName = "Player/PlayerHP")]
public class PlayerHP : ScriptableObject
{
    public int MaxHP;
    public int CurrentHP;
    public int Monedas;
}
