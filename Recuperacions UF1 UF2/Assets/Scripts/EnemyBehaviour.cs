using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    private enum enemystates { Idle, Follow, Attack1, Attack2 }
    private enemystates m_CurrentState;
    public SpriteRenderer enemySpriteRenderer;
    private Animator m_Animator;
    private Transform player;
    private bool persiguiendo = false;
    private Rigidbody2D m_Rigidbody2D;
    void Awake()
    {
        m_Animator = GetComponent<Animator>();
        InitState(enemystates.Idle);
        enemySpriteRenderer = GetComponent<SpriteRenderer>();
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        UpdateState(m_CurrentState);
        if (persiguiendo && player != null)
        {
            Vector3 direccion = (player.position - transform.position).normalized;
            transform.Translate(direccion * 1f * Time.deltaTime);
            float directionSign = Mathf.Sign(direccion.x);
            enemySpriteRenderer.flipX = directionSign < 0;
        }
    }
    public void UnFollow()
    {
        persiguiendo = false;
        m_Rigidbody2D.velocity = Vector3.zero;
        player = null;
        ChangeState(enemystates.Idle);
    }
    public void Unfo() 
    {
        persiguiendo = false;
    }
    public void Attack()
    {
        persiguiendo = false;
        ChangeState(enemystates.Attack1);
    }
    public void Follow(Transform target)
    {
        persiguiendo = true;
        player = target;
        ChangeState(enemystates.Follow);
    }
    private void InitState(enemystates state)
    {
        m_CurrentState = state;
        m_StateDeltaTime = 0;
        switch (m_CurrentState)
        {
            case enemystates.Idle:
                m_Animator.Play("EnemyIdle");
                break;
            case enemystates.Follow:
                m_Animator.Play("EnemyFollow");
                break;
            case enemystates.Attack1:
                m_Animator.Play("EnemyAtack");
                break;
            default:
                break;
        }
    }
    private float m_StateDeltaTime;
    private void UpdateState(enemystates state)
    {
        m_StateDeltaTime += Time.deltaTime;
        switch (m_CurrentState)
        {
            case enemystates.Idle:

                break;
            case enemystates.Follow:
                if (!persiguiendo)
                {
                    ChangeState(enemystates.Idle);
                }
                break;
            case enemystates.Attack1:
                
                if (m_StateDeltaTime > 1f)
                {
                    ChangeState(enemystates.Idle);
                }
                break;
        }
    }
    private void ExitState(enemystates state)
    {
        switch (state)
        {
            case enemystates.Idle:
                break;
            case enemystates.Follow:
                break;
            case enemystates.Attack1:
                break;
            default:
                break;
        }
    }

    private void ChangeState(enemystates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState(m_CurrentState);
        InitState(newState);
    }
}
