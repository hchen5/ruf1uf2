using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Tienda : MonoBehaviour
{
    [SerializeField]
    private GameObject paneltienda;
    [SerializeField]
    private PlayerHP ph;

    public delegate void TiendaGUI(int vida,int monedas);
    public static event TiendaGUI OnTiendaGUI;
    public void ComprarVida() 
    {
        if (ph.CurrentHP +1 <= ph.MaxHP && ph.Monedas >=2) 
        {
            ph.CurrentHP++;
            ph.Monedas -= 2;
            OnTiendaGUI.Invoke(ph.CurrentHP,ph.Monedas);
        }
    }
    public void cerrarTienda() 
    {
        paneltienda.SetActive(false);
    }
    public void abrircanvas() 
    {
        paneltienda.SetActive(!paneltienda.activeSelf);
    }
}
