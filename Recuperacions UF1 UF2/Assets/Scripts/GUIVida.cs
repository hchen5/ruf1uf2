using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GUIVida : MonoBehaviour
{
    private static GUIVida m_Instance;
    public static GUIVida Instance
    {
        get { return m_Instance; }
    }
    private Movement m;
    [SerializeField]
    private TextMeshProUGUI m_Vida;
    [SerializeField]
    private TextMeshProUGUI m_Monedas;
    private void Awake()
    {
       
        if (m_Instance == null)
            m_Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        
        m = FindObjectOfType<Movement>();
    }
    private void Start()
    {
         Movement.OnPlayerGui+= SetVida;
        Tienda.OnTiendaGUI+= SetVida;
    }
    private void SetVida(int vida,int Monedas)
    {
        m_Vida.SetText("Vida: "+vida);
        m_Monedas.SetText("Monedas: "+Monedas);

    }
}
