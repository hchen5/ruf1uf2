using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Inicio : MonoBehaviour
{
    [SerializeField]
    private PlayerHP ph;
    public void changescene() 
    {
        SceneManager.LoadScene("EscenaPrincipal");
        ph.MaxHP = 5;
        ph.CurrentHP = 4;
        ph.Monedas = 6;
    }
}
