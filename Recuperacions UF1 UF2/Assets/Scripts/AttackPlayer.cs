using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPlayer : MonoBehaviour
{
    private EnemyBehaviour enemigo;
    private Coroutine Coroutine;
    [SerializeField]
    private GameEvent ge;
    private void Start()
    {
        enemigo = GetComponentInParent<EnemyBehaviour>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Coroutine = StartCoroutine(Atacar());
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            StopCoroutine(Coroutine);
            enemigo.Follow(collision.transform);
        }
    }
    private IEnumerator Atacar()
    {
        while (true)
        {
            enemigo.Unfo();
            yield return new WaitForSeconds(2f); 
            enemigo.Attack();
            ge.Raise();
        }
    }
}
