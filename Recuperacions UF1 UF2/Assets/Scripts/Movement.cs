using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movement : MonoBehaviour
{
    Rigidbody2D m_RigidBody;
    [SerializeField]
    private float CharacterSpeedWalk = 1.0f;
    [SerializeField]
    private float CharacterSpeedRun = 2.0f;
    private float CharacterSpeedActual = 1.0f;

    private Vector2 InputDir;
    private float VertInput;
    private float HorInput;
    private Vector2 WhereAmI;
    private Vector2 WhereTo;

    [SerializeField]
    private PlayerHP playerHP;
    public bool canMove = true;
    [SerializeField]
    private GameEvent abrirtienda;
    [SerializeField]
    private GameEvent cerrartienda;
    private bool cambiarscene = false;
    public delegate void PlayerGUI(int vida, int monedas);
    public static event PlayerGUI OnPlayerGui;

    private Animator m_Animator;
    void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_Animator= GetComponent<Animator>();
    }
    private void Start()
    {
        StartCoroutine(a());
    }
    private IEnumerator a()
    {
        yield return new WaitForSeconds(1f); OnPlayerGui?.Invoke(playerHP.CurrentHP,playerHP.Monedas);
    }
    public void UpdateAndSetGUI(int vida,int monedas) 
    {
        OnPlayerGui?.Invoke(vida,monedas);
    }
    public void RestarVida()
    {
        if (playerHP.CurrentHP > 0)
        {
            playerHP.CurrentHP--;
            OnPlayerGui.Invoke(playerHP.CurrentHP,playerHP.Monedas);
        }
    }
    private void ReturnToEscenaPrincipal() 
    {
        SceneManager.LoadScene("EscenaPrincipal");
    }
    private GameObject activarBoxAtacar;
    private void Update()
    {
        if (cambiarscene)
        {
            SceneManager.LoadScene("Combat");
        }
        if (tienda) 
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                Debug.Log("GameEventRaise");
                abrirtienda.Raise();
            }
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            StartCoroutine(ActivarDesactivarBoxcolideratacar());
        }
        if (playerHP.CurrentHP ==0) 
        {
            SceneManager.LoadScene("Perdut");
        }
    }
    public void AņadirMonedas(int monedas) 
    {
        playerHP.Monedas += monedas; OnPlayerGui?.Invoke(playerHP.CurrentHP, playerHP.Monedas);
    }
    IEnumerator ActivarDesactivarBoxcolideratacar() 
    {
        Debug.Log("Play");
        //transform.GetChild(0).gameObject.SetActive(true);
        m_Animator.Play("AtacAnim", -1, 0f);
        transform.GetChild(0).transform.position = new Vector3(transform.GetChild(0).transform.position.x, transform.GetChild(0).transform.position.y+0.1f, transform.GetChild(0).transform.position.z);
        yield return new WaitForSeconds(0.5f);
        transform.GetChild(0).transform.position = new Vector3(transform.GetChild(0).transform.position.x, transform.GetChild(0).transform.position.y + -0.1f, transform.GetChild(0).transform.position.z);
        //transform.GetChild(0).gameObject.SetActive(false);
    }
    void FixedUpdate()
    {
        
        if (canMove)
        {
            VertInput = Input.GetAxis("Vertical");
            HorInput = Input.GetAxis("Horizontal");
            if (Input.GetKey(KeyCode.Space))
                CharacterSpeedActual = CharacterSpeedRun;
            else
                CharacterSpeedActual = CharacterSpeedWalk;
            if (InputDir != new Vector2(HorInput, VertInput).normalized)
            {
                InputDir = new Vector2(HorInput, VertInput).normalized;
            }
            WhereAmI = m_RigidBody.position;
            WhereTo = WhereAmI + (InputDir * CharacterSpeedActual) * Time.fixedDeltaTime;
            m_RigidBody.MovePosition(WhereTo);
        }
        

    }
    private bool tienda=false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Combat")
        {
            cambiarscene = true;
        }
        
        if (collision.tag == "EscenaPrincipal")
        {
            SceneManager.LoadScene("EscenaPrincipal");
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Combat")
        {
            cambiarscene = false;
        }
        
        if (collision.tag == "Tienda")
        {
            tienda = false;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Tienda")
        {
            tienda = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == "Tienda")
        {
            tienda = false;
            cerrartienda.Raise();
        }
    }
}
